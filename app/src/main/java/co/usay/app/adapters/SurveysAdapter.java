package co.usay.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.usay.app.R;
import co.usay.app.models.Survey;

/**
 * Created by Ismael on 21/07/2016.
 */
public class SurveysAdapter extends RecyclerView.Adapter<SurveysAdapter.ViewHolder> {

    private List<Survey> mSurveys;

    private ItemClickListener mItemClickListener;

    public SurveysAdapter(List<Survey> surveys, ItemClickListener itemClickListener) {
        mSurveys = surveys;
        mItemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.survey_row_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Survey survey = mSurveys.get(position);
        holder.titleView.setText(survey.getTitle());
        holder.descriptionView.setText(survey.getDescription());
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onButtonClicked(survey);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSurveys.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView titleView;
        public final TextView descriptionView;
        public final TextView btnView;

        public ViewHolder(View itemView) {
            super(itemView);
            titleView = (TextView) itemView.findViewById(R.id.survey_title);
            descriptionView = (TextView) itemView.findViewById(R.id.survey_description);
            btnView = (TextView) itemView.findViewById(R.id.survey_btn);
        }

        public void setOnClickListener(View.OnClickListener listener) {
            btnView.setOnClickListener(listener);
        }

    }

    public interface ItemClickListener {
        void onButtonClicked(Survey survey);
    }

}