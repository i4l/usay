package co.usay.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import co.usay.app.models.Survey;

public class SurveyActivity extends AppCompatActivity {

    public static final String SURVEY = "survey";

    private ImageView mSurveyImageView;

    private Survey mSurvey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mSurveyImageView = (ImageView) findViewById(R.id.survey_image);

        mSurvey = getIntent().getParcelableExtra(SURVEY);

        if (mSurvey != null) {
            Picasso.with(this).load(mSurvey.getCoverImageUrl() + "l").into(mSurveyImageView);
        }
    }
}
