package co.usay.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bignerdranch.android.multiselector.SingleSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import co.usay.app.adapters.SurveysAdapter;
import co.usay.app.models.Survey;
import co.usay.app.network.ErrorBundle;
import co.usay.app.network.WebService;
import co.usay.app.utils.NetworkHelper;
import co.usay.app.views.DividerItemDecoration;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SurveysActivity extends AppCompatActivity
        implements SurveysAdapter.ItemClickListener {

    private static final String SURVEYS_KEY = "surveys";
    private static final String SELECTED_SURVEY_POSITION_KEY = "selected_survey_position";

    private ActionMenuView mActionMenuView;
    private RecyclerView mDotsRecyclerView;
    private RecyclerView mSurveysRecyclerView;
    private ImageView mSurveyImageView;
    private View mErrorView;
    private ProgressBar mProgressBarView;
    private TextView mErrorMessageView;

    private ArrayList<Survey> mSurveys;

    private DotsAdapyer mDotsAdapter;
    private SurveysAdapter mSurveysAdapter;

    private Retrofit mRetrofit;
    private WebService mWebService;

    private RecyclerView.OnScrollListener mSurveysRecyclerViewListener;

    private SingleSelector mDotsSingleSelector = new SingleSelector();

    private int mSelectedSurveyPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surveys);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        mActionMenuView = (ActionMenuView) toolbar.findViewById(R.id.action_menu);
        mActionMenuView.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return onOptionsItemSelected(item);
            }
        });

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mSurveyImageView = (ImageView) findViewById(R.id.survey_image);
        mDotsRecyclerView = (RecyclerView) findViewById(R.id.dots_recycler_view);
        mSurveysRecyclerView = (RecyclerView) findViewById(R.id.surveys_recycler_view);
        mErrorView = findViewById(R.id.error_view);
        mProgressBarView = (ProgressBar) findViewById(R.id.progress_bar);
        mErrorMessageView = (TextView) findViewById(R.id.error_message);

        mDotsRecyclerView.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mSurveysRecyclerView.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        mSurveysRecyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL_LIST));

        mSurveys = new ArrayList<>();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(0, TimeUnit.MINUTES)
                .readTimeout(0, TimeUnit.MINUTES)
                .build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(WebService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        mWebService = mRetrofit.create(WebService.class);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(SURVEYS_KEY)) {
                mSurveys = savedInstanceState.getParcelableArrayList(SURVEYS_KEY);
                mSelectedSurveyPosition = savedInstanceState.getInt(SELECTED_SURVEY_POSITION_KEY, 0);
                showSurveys(mSelectedSurveyPosition);
            }
        } else {
            fetchSurveys();
        }

        mSurveysRecyclerViewListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int position = mSurveysRecyclerView
                        .getChildAdapterPosition(recyclerView.findChildViewUnder(dx, dy));
                // Avoid invalid target position
                if (position > -1) {
                    // Select the corresponding dot and load cover image
                    mDotsSingleSelector.setSelected(position, mDotsAdapter.getItemId(position),
                            true);
                    mSelectedSurveyPosition = position;
                    Picasso.with(SurveysActivity.this).load(mSurveys.get(position)
                            .getCoverImageUrl() + "l").into(mSurveyImageView);
                    mDotsRecyclerView.smoothScrollToPosition(position);
                }
            }
        };
        mSurveysRecyclerView.addOnScrollListener(mSurveysRecyclerViewListener);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(SURVEYS_KEY, mSurveys);
        outState.putInt(SELECTED_SURVEY_POSITION_KEY, mSelectedSurveyPosition);
        super.onSaveInstanceState(outState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_surveys, mActionMenuView.getMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            // Clear the screen
            mSurveys.clear();
            mSurveyImageView.setImageDrawable(null);
            mSurveysRecyclerView.setAdapter(null);
            mDotsRecyclerView.setAdapter(null);
            // Fetch content
            fetchSurveys();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void fetchSurveys() {
        // Remove previous error message
        mErrorMessageView.setText("");
        setVisibility(mErrorMessageView, mErrorView, mProgressBarView, false);

        // Check connectivity
        if (!NetworkHelper.isOnline(this)) {
            mErrorMessageView.setText(R.string.error_no_connection);
            setVisibility(mErrorMessageView, mErrorView, mProgressBarView, true);
            return;
        }

        showProgressBar(true);

        Call<ArrayList<Survey>> callSurveys = mWebService.getSurveys();

        callSurveys.enqueue(new Callback<ArrayList<Survey>>() {
            @Override
            public void onResponse(Call<ArrayList<Survey>> call, Response<ArrayList<Survey>> response) {
                showProgressBar(false);
                if (response.isSuccessful()) {
                    mSurveys = response.body();
                    showSurveys(0);
                } else {
                    int statusCode = response.code();
                    ResponseBody errorBody = response.errorBody();
                    mErrorMessageView.setText(String.format("Error %s: %s", statusCode,
                            errorBody.toString()));
                    setVisibility(mErrorMessageView, mErrorView, mProgressBarView, true);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Survey>> call, Throwable t) {
                showProgressBar(false);
                ErrorBundle errorBundle = ErrorBundle.adapt(t);
                String error = errorBundle.getRawMessage() != null ?
                        String.format("%s %s", errorBundle.getAppMessage(),
                                errorBundle.getRawMessage()) :
                        errorBundle.getAppMessage();
                mErrorMessageView.setText(error);
                setVisibility(mErrorMessageView, mErrorView, mProgressBarView, true);
            }
        });
    }

    @Override
    public void onButtonClicked(Survey survey) {
        Intent intent = new Intent(this, SurveyActivity.class)
                .putExtra(SurveyActivity.SURVEY, survey);
        startActivity(intent);
    }

    private class DotsViewHolder extends SwappingHolder implements View.OnClickListener {

        private Survey mSurvey;

        public DotsViewHolder(View itemView) {
            super(itemView, mDotsSingleSelector);
            itemView.setOnClickListener(this);
        }

        public void bindSurvey(Survey survey) {
            mSurvey = survey;
        }

        @Override
        public void onClick(View view) {
            // Select dot, move to the corresponding survey and load cover image
            mDotsSingleSelector.setSelected(this, true);
            int position = this.getAdapterPosition();
            mSelectedSurveyPosition = position;
            Picasso.with(SurveysActivity.this).load(mSurvey.getCoverImageUrl() + "l")
                    .into(mSurveyImageView);
            mSurveysRecyclerView.smoothScrollToPosition(position);
        }

    }

    public class DotsAdapyer extends RecyclerView.Adapter<DotsViewHolder> {

        private List<Survey> mSurveys;

        public DotsAdapyer(List<Survey> surveys) {
            mSurveys = surveys;
        }

        @Override
        public DotsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dot_row_item, parent, false);
            return new DotsViewHolder(view);
        }

        @Override
        public void onBindViewHolder(DotsViewHolder holder, int position) {
            Survey survey = mSurveys.get(position);
            holder.bindSurvey(survey);
        }

        @Override
        public int getItemCount() {
            return mSurveys.size();
        }

    }

    private void showProgressBar(boolean show) {
        setVisibility(mProgressBarView, mErrorView, mErrorMessageView, show);
    }

    private void setVisibility(View view, View parent, View siblingView, boolean visible) {
        if (visible) {
            if (parent.getVisibility() == View.GONE) {
                parent.setVisibility(View.VISIBLE);
            }
            if (view.getVisibility() == View.GONE) {
                view.setVisibility(View.VISIBLE);
            }
        } else {
            if (view.getVisibility() == View.VISIBLE) {
                view.setVisibility(View.GONE);
            }
            if (parent.getVisibility() == View.VISIBLE
                    && siblingView.getVisibility() == View.GONE) {
                parent.setVisibility(View.GONE);
            }
        }
    }

    private void showSurveys(int selectedSurveyPosition) {
        setAdapters(mSurveys);

        // TODO check no results

        mDotsSingleSelector.setSelected(selectedSurveyPosition,
                mDotsAdapter.getItemId(selectedSurveyPosition), true);
        mSelectedSurveyPosition = selectedSurveyPosition;
        Picasso.with(SurveysActivity.this).load(mSurveys.get(selectedSurveyPosition)
                .getCoverImageUrl() + "l").into(mSurveyImageView);
    }

    private void setAdapters(List<Survey> surveys) {
        mDotsAdapter = new DotsAdapyer(surveys);
        mDotsRecyclerView.setAdapter(mDotsAdapter);

        mSurveysAdapter = new SurveysAdapter(surveys, this);
        mSurveysRecyclerView.setAdapter(mSurveysAdapter);
    }

}
