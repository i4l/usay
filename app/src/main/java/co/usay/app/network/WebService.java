package co.usay.app.network;

import java.util.ArrayList;

import co.usay.app.models.Survey;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Ismael on 21/07/2016.
 */
public interface WebService {

    String BASE_URL = "https://www-staging.usay.co/app/";

    @GET("surveys.json?access_token=6eebeac3dd1dc9c97a06985b6480471211a777b39aa4d0e03747ce6acc4a3369")
    Call<ArrayList<Survey>> getSurveys();

}
