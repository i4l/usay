package co.usay.app.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.net.SocketTimeoutException;

/**
 * Created by Ismael on 21/07/2016.
 */
public class ErrorBundle {

    private final String appMessage;
    private final String rawMessage;

    private ErrorBundle(String message, String reason) {
        this.appMessage = message;
        this.rawMessage = reason;
    }

    @NonNull
    public String getAppMessage() {
        return appMessage;
    }

    @Nullable
    public String getRawMessage() {
        return rawMessage;
    }

    @NonNull
    public static ErrorBundle adapt(Throwable throwable) {
        if (throwable instanceof SocketTimeoutException) {
            String reason = null;
            if (throwable.getCause() != null) {
                reason = throwable.getCause().getMessage();
            }
            if (reason == null) {
                reason = "Connection timeout";
            }
            return new ErrorBundle("Socket timeout", reason);
        }
        if (throwable instanceof AuthorizeException) {
            return new ErrorBundle("Authorization exception", null);
        }
        return new ErrorBundle("Unknown exception", throwable.getMessage());
    }

}
