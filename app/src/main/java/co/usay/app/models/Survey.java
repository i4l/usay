package co.usay.app.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ismael on 20/07/2016.
 */
public class Survey implements Parcelable {

    private String id;
    private String title;
    private String description;
    private String cover_image_url;

    public Survey() {}

    public Survey(String id, String title, String description, String cover_image_url) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.cover_image_url = cover_image_url;
    }

    protected Survey(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        cover_image_url = in.readString();
    }

    public static final Creator<Survey> CREATOR = new Creator<Survey>() {
        @Override
        public Survey createFromParcel(Parcel in) {
            return new Survey(in);
        }

        @Override
        public Survey[] newArray(int size) {
            return new Survey[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCoverImageUrl() {
        return cover_image_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(cover_image_url);
    }
}
